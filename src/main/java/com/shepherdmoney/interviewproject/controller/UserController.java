package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.CreditCardRepository;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.vo.request.CreateUserPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class UserController {

    // TODO: wire in the user repository (~ 1 line)
    @Autowired
    UserRepository userRepository;

    /*
    * This method is used to create a new user. The payload contains the user's name and email.
    * @param payload The CreateUserPayload object
    * @return A ResponseEntity with a status code of 200 and the user ID in the body if the user was successfully created.
    */
    @PutMapping("/user")
    public ResponseEntity<Integer> createUser(@RequestBody CreateUserPayload payload) {
        User newUser = new User();
        newUser.setEmail(payload.getEmail());
        newUser.setName(payload.getName());
        userRepository.save(newUser);

        return ResponseEntity.ok(newUser.getId()); // If all has gone well return a 200 with the newUser ID

    }

    /*
    * This method is used to delete a user. The user ID is passed as a query parameter.
    * @param userId The user ID
    * @return A ResponseEntity with a status code of 200 if the user was successfully deleted, or a status code of 404 if the user ID is not found.
    */
    @DeleteMapping("/user")
    public ResponseEntity<String> deleteUser(@RequestParam int userId) {
        // TODO: Return 200 OK if a user with the given ID exists, and the deletion is successful
        //       Return 400 Bad Request if a user with the ID does not exist
        //       The response body could be anything you consider appropriate
        Optional<User> deleteUser = userRepository.findById(userId);
        if(deleteUser.isEmpty())
        {
            return new ResponseEntity<>(Integer.toString(userId), HttpStatusCode.valueOf(404)); // Send 404 Error if not found
        }
        userRepository.delete(deleteUser.get());
        return ResponseEntity.ok(Integer.toString(deleteUser.get().getId()));
    }
}
