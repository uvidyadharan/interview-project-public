package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.BalanceHistory;
import com.shepherdmoney.interviewproject.model.CreditCard;
import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.CreditCardRepository;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.vo.request.AddCreditCardToUserPayload;
import com.shepherdmoney.interviewproject.vo.request.UpdateBalancePayload;
import com.shepherdmoney.interviewproject.vo.response.CreditCardView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
public class CreditCardController {

    @Autowired
    CreditCardRepository creditCardRepository;

    @Autowired
    UserRepository userRepository;

    /*
    * This method is used to add a credit card to a user. The payload contains the user ID, the credit card number, and the issuing bank.
    * @param payload The AddCreditCardToUserPayload object
    * @return A ResponseEntity with a status code of 200 and the credit card ID in the body if the credit card was successfully added, or a status code of 404 if the user ID is not found.
    */
    @PostMapping("/credit-card")
    public ResponseEntity<Integer> addCreditCardToUser(@RequestBody AddCreditCardToUserPayload payload) {
        // Retrieve User
        Optional<User> optionalUser = userRepository.findById(payload.getUserId());
        if (optionalUser.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        // Create new credit card
        CreditCard card = new CreditCard();
        card.setIssuanceBank(payload.getCardIssuanceBank());
        card.setNumber(payload.getCardNumber());
        card.setOwner(optionalUser.get());
        creditCardRepository.save(card);

        // Return new credit card ID
        return ResponseEntity.ok(card.getId());

    }

    /*
    * This method is used to get all credit cards associated with a user ID.
    * @param userId The user ID
    * @return A ResponseEntity with a status code of 200 and a list of CreditCardView objects in the body if the user ID is found, or a status code of 404 if the user ID is not found.
    */

    @GetMapping("/credit-card:all")
    public ResponseEntity<List<CreditCardView>> getAllCardOfUser(@RequestParam int userId) {
        // TODO: return a list of all credit card associated with the given userId, using CreditCardView class
        //       if the user has no credit card, return empty list, never return null
        // Search for User
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty())
        {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatusCode.valueOf(404)); // Send 404 Error if not found with empty list
        }

        // Map Credit Cards to CreditCardView
        List<CreditCardView> views = user.get().getCreditCards().stream()
                .map(creditCard -> new CreditCardView(creditCard.getIssuanceBank(), creditCard.getNumber()))
                .toList();

        // Return with 200
        return ResponseEntity.ok(views);

    }

    /*
    * This method is used to get the user ID associated with a credit card number.
    * @param creditCardNumber The credit card number
    * @return A ResponseEntity with a status code of 200 and the user ID in the body if the credit card number is found, or a status code of 400 if the credit card number is not found.
    */
    @GetMapping("/credit-card:user-id")
    public ResponseEntity<Integer> getUserIdForCreditCard(@RequestParam String creditCardNumber) {
        // Search for Credit Card by Number and return the User ID
        Optional<CreditCard> creditCard = creditCardRepository.findByNumber(creditCardNumber);
        return creditCard.map(card -> ResponseEntity.ok(card.getOwner().getId())).orElseGet(() -> ResponseEntity.badRequest().build());
    }

    /*
    * This method is used to update the balance of a credit card. The payload is an array of UpdateBalancePayload objects.
    * Each object contains the credit card number, the balance amount, and the balance date.
    * The balance amount can be positive or negative, and the balance date is the date when the balance change occurred.
    * There is no overdraft protection
    * @param payload An array of UpdateBalancePayload objects
    * @return A ResponseEntity with a status code of 200 if the update was successful, or a status code of 400 if the credit card number is not found.
    */

    @PostMapping("/credit-card:update-balance")
    public ResponseEntity<Void> postMethodName(@RequestBody UpdateBalancePayload[] payload) {
        

        // Sort payload transaction list chronologically
        Arrays.sort(payload, Comparator.comparing(UpdateBalancePayload::getBalanceDate));

        // Iterate through each transaction
        for(UpdateBalancePayload entry: payload)
        {
            // Find Credit Card associated with the transaction
            Optional<CreditCard> assocCard = creditCardRepository.findByNumber(entry.getCreditCardNumber());

            if(assocCard.isEmpty())
            {
                return ResponseEntity.badRequest().build();
            }

            // Get Credit Card and Balance History
            CreditCard creditCard = assocCard.get();
            List<BalanceHistory> balanceHistories = creditCard.getBalanceHistory();
            Optional<BalanceHistory> joinedHistory = creditCard
                    .getBalanceHistory().stream()
                    .filter(history -> history.getDate().equals(entry.getBalanceDate()))
                    .findFirst();

            // If history for the given date is not found, create a new one
            BalanceHistory balanceHistory = joinedHistory.orElseGet(() -> {
                BalanceHistory newHistory = new BalanceHistory();
                newHistory.setDate(entry.getBalanceDate());
                newHistory.setCreditCard(creditCard);
                balanceHistories.add(newHistory);
                return newHistory;
            });

            // Update the balance amount for the found/created history
            balanceHistory.setBalance(balanceHistory.getBalance() + entry.getBalanceAmount());

            // Save the updated credit card entity
            creditCardRepository.save(creditCard);
        }
        return ResponseEntity.ok().build();
    }
    
}
