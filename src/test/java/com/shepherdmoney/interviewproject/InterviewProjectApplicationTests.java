package com.shepherdmoney.interviewproject.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shepherdmoney.interviewproject.model.CreditCard;
import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.CreditCardRepository;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.vo.request.AddCreditCardToUserPayload;
import com.shepherdmoney.interviewproject.vo.request.CreateUserPayload;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class InterviewProjectApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CreditCardRepository creditCardRepository;

    @Test
    void testCreateUser() throws Exception {
        CreateUserPayload payload = new CreateUserPayload();
        payload.setName("John Doe");
        payload.setEmail("john@example.com");

        mockMvc.perform(MockMvcRequestBuilders.put("/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(payload)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    // Utility method to convert object to JSON string
    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Transactional // Needed to prevent LazyInitializationException
    void testAddCreditCardToUser() throws Exception {
        User user = new User();
        user.setName("Jane Doe");
        user.setEmail("jane@example.com");
        userRepository.save(user);

        AddCreditCardToUserPayload payload = new AddCreditCardToUserPayload();
        payload.setUserId(user.getId());
        payload.setCardIssuanceBank("Test Bank");
        payload.setCardNumber("9959");

        mockMvc.perform(MockMvcRequestBuilders.post("/credit-card")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(payload)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        Optional<CreditCard> savedCard = creditCardRepository.findByNumber("9959");
        assertThat(savedCard).isPresent();
        assertThat(savedCard.get().getOwner()).isEqualTo(user);
    }

   @Test
   @Transactional
    void testGetUserIdForCreditCard() throws Exception {
    // Create a user
    User user = new User();
    user.setName("James Doe");
    user.setEmail("James@example.com");
    userRepository.save(user);

    // Retrieve the user from the database within the current session
    user = userRepository.findById(user.getId()).orElseThrow();

    // Create a credit card associated with the user
    CreditCard creditCard = new CreditCard();
    creditCard.setIssuanceBank("Test Bank");
    creditCard.setNumber("1187");
    creditCard.setOwner(user);
    creditCardRepository.save(creditCard);

    // Perform the test
    mockMvc.perform(MockMvcRequestBuilders.get("/credit-card:user-id")
                    .param("creditCardNumber", creditCard.getNumber()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    


}
