# Uday Vidyadharan - Shepherd Money Interview Project

``I have used generative AI tools in the process of completing this project.``

Testing Suite
-------------

I have built a basis for a testing suite to show how I would set the unit tests up. 
The suite is not comprehensive. It tests the following

- Create User
- Add Credit Card to User
- Get User ID for Credit Card

To run tests

``./gradlew test``

To run full setup

``./gradlew bootRun``

For more comprehensive testing I used postman to create custom requests and see the 
on the database. The built-in testing is automatically run using the GitLab CI/CD Pipeline.